package com.realmofwrath.game.inventory;

public enum Attribute {

	STACKABLE(0, "A measure of how many items can be stacked in one inventory slot");

	private final int id;
	private final String description;

	private Attribute(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public static Attribute getAttribute(int id) {
		for (Attribute attribute : values()) {
			if (attribute.id == id) {
				return attribute;
			}
		}
		return null;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return toString().substring(0, 1) + toString().substring(1).toLowerCase();
	}

}