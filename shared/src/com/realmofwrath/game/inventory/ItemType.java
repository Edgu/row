package com.realmofwrath.game.inventory;

public enum ItemType {
	
	DEFAULT,
	NECKLACE,
	HELMET,
	BACKPACK,
	WEAPON,
	ARMOUR,
	SHIELD,
	BRACELET,
	PANTS,
	RING,
	GLOVES,
	BELT,
	BOOTS;

	public static ItemType getType(int type) {
		for (ItemType typeValue : values()) {
			if (typeValue.ordinal() == type) {
				return typeValue;
			}
		}
		return null;
	}

}