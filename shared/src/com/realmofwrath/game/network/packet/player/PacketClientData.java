package com.realmofwrath.game.network.packet.player;

public class PacketClientData {

	public int id;
	public String name;
	public int mapId;
	public int x, y;

}