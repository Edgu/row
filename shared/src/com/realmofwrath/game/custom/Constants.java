package com.realmofwrath.game.custom;

public class Constants {

	// MOVEMENT
	public static final int MOVEMENT_SPEED = 1;
	public static final int TILE_SIZE = 32;

	// INVENTORY
	public static final int INVENTORY_EQUIPMENT_SLOTS_AMOUNT = 12;
	public static final int INVENTORY_SLOTS_AMOUNT = INVENTORY_EQUIPMENT_SLOTS_AMOUNT + 6; // + 24 max
	
	// LOOT
	public static final int DROP_BAG_SLOTS_AMOUNT = 6;
	public static final int DROP_BAG_DISTANCE = 4;
}
