package com.realmofwrath.game.custom;

import java.util.Comparator;

public class Node implements Comparable<Node> {
	
	public static Comparator<Node> nodeComparator = new Comparator<Node>() {
		@Override
		public int compare(Node node0, Node node1) {
			return node0.compareTo(node1);
		}
	};

	public Vector2Integer position;
	public Node parent;
	/*
	 * hCost (Heuristic) - Cost from a node to the target node.
	 * gCost - Cost from parent plus this node.
	 * fCost - gCost + hCost
	 */
	public double fCost, gCost, hCost;

	public Node(Vector2Integer position, Node parent, double gCost, double hCost) {
		this.position = position;
		this.parent = parent;
		this.gCost = gCost;
		this.hCost = hCost;
		this.fCost = gCost + hCost;
	}

	@Override
	public int compareTo(Node otherNode) {
		if (fCost > otherNode.fCost) {
			return 1;
		} else if (fCost < otherNode.fCost) {
			return -1;
		}
		return 0;
	}

	public String toString() {
		return position.toString();
	}

}