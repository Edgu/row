package com.realmofwrath.game.custom;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Vector3;

public class TiledUtils {

	public static final StaticTiledMapTile TREE;
	public static final StaticTiledMapTile TREE_STUMP;

	public static final StaticTiledMapTile ROCK;
	public static final StaticTiledMapTile ROCK_MINED;

	public static final StaticTiledMapTile FISH;
	public static final StaticTiledMapTile FISH_DEPLETED;

	public static void init() {

	}

	static {
		TREE = new StaticTiledMapTile(new TextureRegion(new Texture(Gdx.files.internal("maps/tree.png"))));
		TREE.getProperties().put("solid", true);
		TREE.getProperties().put("name", "tree");
		TREE.getProperties().put("resource", true);

		TREE_STUMP = new StaticTiledMapTile(new TextureRegion(new Texture(Gdx.files.internal("maps/tree_stump.png"))));
		TREE_STUMP.getProperties().put("solid", true);
		TREE_STUMP.getProperties().put("name", "tree_stump");

		ROCK = new StaticTiledMapTile(new TextureRegion(new Texture(Gdx.files.internal("maps/rock.png"))));
		ROCK.getProperties().put("solid", true);
		ROCK.getProperties().put("name", "rock");
		ROCK.getProperties().put("resource", true);

		ROCK_MINED = new StaticTiledMapTile(new TextureRegion(new Texture(Gdx.files.internal("maps/rock_mined.png"))));
		ROCK_MINED.getProperties().put("solid", true);
		ROCK_MINED.getProperties().put("name", "rock_mined");

		FISH = new StaticTiledMapTile(new TextureRegion(new Texture(Gdx.files.internal("maps/fish.png"))));
		FISH.getProperties().put("solid", true);
		FISH.getProperties().put("name", "fish");
		FISH.getProperties().put("resource", true);

		FISH_DEPLETED = new StaticTiledMapTile(new TextureRegion(new Texture(Gdx.files.internal("maps/fish_depleted.png"))));
		FISH_DEPLETED.getProperties().put("solid", true);
		FISH_DEPLETED.getProperties().put("name", "fish_depleted");
	}

	public static Vector2Integer positionToTiled(int x, int y) {
		return new Vector2Integer((int) x >> 5, (int) y >> 5);
	}

	public static Vector2Integer positionToTiled(Vector2Integer position) {
		return positionToTiled(position.x, position.y);
	}

	public static Vector2Integer positionToTiled(Vector3 position) {
		return positionToTiled((int) position.x, (int) position.y);
	}

	public static Vector2Integer positionToMap(int x, int y) {
		return new Vector2Integer(x << 5, y << 5);
	}

	public static Vector2Integer positionToMap(Vector2Integer position) {
		return positionToMap(position.x, position.y);
	}

	public static TiledMapTile getTile(TiledMapTileLayer layer, int x, int y) {
		Cell cell = layer.getCell(x, y);
		return (cell != null) ? cell.getTile() : null;
	}

	public static TiledMapTile getTile(TiledMapTileLayer layer, Vector2Integer position) {
		return getTile(layer, position.x, position.y);
	}

	public static boolean isTileSolid(TiledMapTile tile) {
		return tile.getProperties().containsKey("solid");
	}

	public static boolean setTile(TiledMapTileLayer layer, TiledMapTile tile, int x, int y) {
		return layer.getCell(x, y).setTile(tile) != null;
	}

	public static boolean setTile(TiledMapTileLayer layer, TiledMapTile tile, Vector2Integer position) {
		return setTile(layer, tile, position.x, position.y);
	}

}