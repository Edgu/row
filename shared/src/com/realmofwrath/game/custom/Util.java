package com.realmofwrath.game.custom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.maps.tiled.TiledMapTile;

public abstract class Util {

	// A* Path Finding Algorithm
	public static List<Node> findPath(Map map, Vector2Integer start, Vector2Integer finish, boolean diagonal, boolean excludeFinish) {
		long current = System.nanoTime();
		// List of nodes that need to be checked.
		List<Node> openList = new ArrayList<Node>();
		// List of nodes that have been checked.
		List<Node> closedList = new ArrayList<Node>();
		Node currentNode = new Node(start, null, 0, Vector2Integer.euclideanDistance(start, finish));
		openList.add(currentNode);

		// Search for path, if openList is empty means there is no path.
		int counter = 0;
		while (openList.size() > 0) {
			Collections.sort(openList, Node.nodeComparator);
			currentNode = openList.get(0);
			if (finish.equals(currentNode.position)) {
				List<Node> path = new ArrayList<Node>();
				while (currentNode.parent != null) {
					path.add(currentNode);
					currentNode = currentNode.parent;
				}
				openList.clear();
				closedList.clear();
//				System.out.println(counter);
//				System.out.println("[findPath] FOUND: " + (System.nanoTime() - current) / 1000000f + " ms.");
				return path;
			}
			openList.remove(currentNode);
			closedList.add(currentNode);

			for (int i = 0; i < 9; i++) {
				// If diagonal is disabled, skip every diagonal tile.
				if (!diagonal && i % 2 == 0) {
					continue;
				}

				// Skip middle tile player is currently at.
				if (i == 4) {
					continue;
				}

				// Adjacent tile
				Vector2Integer at = new Vector2Integer(currentNode.position.x + ((i % 3) - 1), currentNode.position.y + ((i / 3) - 1));

				if (!excludeFinish || !at.equals(finish)) {
					TiledMapTile atTile = TiledUtils.getTile(map.getMainLayer(), at.x, at.y);
					if (atTile == null || TiledUtils.isTileSolid(atTile)) continue;

					TiledMapTile atObjectTile = TiledUtils.getTile(map.getTileObjectLayer(), at.x, at.y);
					if (atObjectTile != null && TiledUtils.isTileSolid(atObjectTile)) continue;

					// Diagonal movement
					if (i % 2 == 0) {
						int xDiagonal = 0;
						int yDiagonal = 0;

						switch (i) {
						case 0:
							xDiagonal = at.x + 1;
							yDiagonal = at.y + 1;
							break;
						case 2:
							xDiagonal = at.x - 1;
							yDiagonal = at.y + 1;
							break;
						case 6:
							xDiagonal = at.x + 1;
							yDiagonal = at.y - 1;
							break;
						case 8:
							xDiagonal = at.x - 1;
							yDiagonal = at.y - 1;
							break;
						}

						TiledMapTile xDiagTile = TiledUtils.getTile(map.getMainLayer(), xDiagonal, at.y);
						if (xDiagTile == null || TiledUtils.isTileSolid(xDiagTile)) continue;

						TiledMapTile yDiagTile = TiledUtils.getTile(map.getMainLayer(), at.x, yDiagonal);
						if (yDiagTile == null || TiledUtils.isTileSolid(yDiagTile)) continue;

						TiledMapTile xObjectDiagTile = TiledUtils.getTile(map.getTileObjectLayer(), xDiagonal, at.y);
						if (xObjectDiagTile != null && TiledUtils.isTileSolid(xObjectDiagTile)) continue;

						TiledMapTile yObjectDiagTile = TiledUtils.getTile(map.getTileObjectLayer(), at.x, yDiagonal);
						if (yObjectDiagTile != null && TiledUtils.isTileSolid(yObjectDiagTile)) continue;
					}
				}

				double gCost = currentNode.gCost + Vector2Integer.euclideanDistance(currentNode.position, at);

				if (isVectorInList(closedList, at) && gCost >= currentNode.gCost) {
					continue;
				}

				if (!isVectorInList(openList, at) || gCost < currentNode.gCost) {
					openList.add(new Node(at, currentNode, gCost, Vector2Integer.euclideanDistance(at, finish) == 1 ? 1 : 0.9));
//					counter++;
				}
			}
		}
		closedList.clear();
//		System.out.println("[findPath] NOT FOUND: " + (System.nanoTime() - current) / 1000000f);
		return new ArrayList<Node>();
	}

	public static boolean raycast(Map map, double startX, double startY, double endX, double endY) {
		startX += 0.5f;
		startY += 0.5f;
		endX += 0.5f;
		endY += 0.5f;

		double tForNextBorderX, tForNextBorderY;
		double xDirection = endX - startX;
		double yDirection = endY - startY;
		double tForOneX = Math.abs(1.0 / xDirection);
		double tForOneY = Math.abs(1.0 / yDirection);
		double yStep = (yDirection >= 0) ? 1 : -1;
		double xStep = (xDirection >= 0) ? 1 : -1;
		double tValue = 0;
		double xGrid = Math.floor(startX);
		double yGrid = Math.floor(startY);

		double fracStartPosX = startX - Math.floor(startX);
		if (xDirection > 0) {
			tForNextBorderX = (1 - fracStartPosX) * tForOneX;
		} else {
			tForNextBorderX = fracStartPosX * tForOneX;
		}

		double fracStartPosY = startY - Math.floor(startY);
		if (yDirection > 0) {
			tForNextBorderY = (1 - fracStartPosY) * tForOneY;
		} else {
			tForNextBorderY = fracStartPosY * tForOneY;
		}

		while (tValue <= 1f) {
			double x = xGrid;
			double y = yGrid;

			if (Math.abs(tForNextBorderX - tForNextBorderY) < 0.000001) {
				tValue = tForNextBorderX;
				tForNextBorderX += tForOneX;
				tForNextBorderY += tForOneY;
				xGrid += xStep;
				yGrid += yStep;
			} else if (tForNextBorderX < tForNextBorderY) {
				tValue = tForNextBorderX;
				tForNextBorderX += tForOneX;
				xGrid += xStep;
			} else {
				tValue = tForNextBorderY;
				tForNextBorderY += tForOneY;
				yGrid += yStep;
			}

			TiledMapTile tile = TiledUtils.getTile(map.getMainLayer(), (int) x, (int) y);

			if (tile == null || TiledUtils.isTileSolid(tile))  {
				return false;
			}

			TiledMapTile objectTile = TiledUtils.getTile(map.getTileObjectLayer(), (int) x, (int) y);

			if (objectTile != null && TiledUtils.isTileSolid(objectTile)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isVectorInList(List<Node> list, Vector2Integer vector) {
		for (Node node : list) {
			if (vector.equals(node.position)) {
				return true;
			}
		}
		return false;
	}

	public static int clamp(int value, int min, int max) {
		return Math.max(min, Math.min(max, value));
	}

	public static Vector2Integer directionToMove(int direction) {
		switch(Direction.getDirection(direction)) {
		case NORTH:
			return new Vector2Integer(0, 1);
		case NORTH_EAST:
			return new Vector2Integer(1, 1);
		case EAST:
			return new Vector2Integer(1, 0);
		case SOUTH_EAST:
			return new Vector2Integer(1, -1);
		case SOUTH:
			return new Vector2Integer(0, -1);
		case SOUTH_WEST:
			return new Vector2Integer(-1, -1);
		case WEST:
			return new Vector2Integer(-1, 0);
		case NORTH_WEST:
			return new Vector2Integer(-1, 1);
		default:
			return null;
		}
	}

	public static Direction velocityToDirection(Vector2Integer vector) {
		switch (vector.x) {
		case -1:
			if (vector.y < 0) return Direction.SOUTH_WEST;
			else if (vector.y > 0) return Direction.NORTH_WEST;
			return Direction.WEST;
		case 0:
			if (vector.y < 0) return Direction.SOUTH;
			return Direction.NORTH;
		case 1:
			if (vector.y < 0) return Direction.SOUTH_EAST;
			else if (vector.y > 0) return Direction.NORTH_EAST;
			return Direction.EAST;
		default:
			return null;
		}
	}

}