package com.realmofwrath.game.custom;

import com.badlogic.gdx.math.Vector3;

public class Vector2Integer {

	public int x, y;

	public Vector2Integer(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Vector2Integer(Vector2Integer vector) {
		this(vector.x, vector.y);
	}
	
	public Vector2Integer(Vector3 vector) {
		this((int) vector.x, (int) vector.y);
	}

	public boolean equals(Object object) {
		if (object instanceof Vector2Integer && object != null) {
			Vector2Integer other = (Vector2Integer) object;
			return equals(other.x, other.y);
		}
		return false;
	}

	public boolean equals(Vector2Integer other) {
		return other != null && equals(other.x, other.y);
	}

	public boolean equals(int x, int y) {
		return this.x == x && this.y == y;
	}
	
	public void set(Vector2Integer other) {
		x = other.x;
		y = other.y;
	}

	@Override
	public String toString() {
		return "[" + x + ", " + y + "]";
	}
	
	public static int chebyshevDistance(Vector2Integer start, Vector2Integer end) {
		return Math.max(Math.abs(end.x - start.x), Math.abs(end.y - start.y));
	}

	public static double euclideanDistance(Vector2Integer start, Vector2Integer end) {
		return Math.sqrt(Math.pow(end.x - start.x, 2) + Math.pow(end.y - start.y, 2));
	}

}