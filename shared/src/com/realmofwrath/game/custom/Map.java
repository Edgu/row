package com.realmofwrath.game.custom;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public abstract class Map {

	private int id;
	private TiledMap tiledMap;
	private TiledMapTileLayer mainLayer;
	private TiledMapTileLayer tileObjectLayer;
	private MapLayer objectLayer;

	public Map(int id, TiledMap tiledMap) {
		this.id = id;
		this.tiledMap = tiledMap;
		mainLayer = (TiledMapTileLayer) tiledMap.getLayers().get("mainLayer");
		tileObjectLayer = (TiledMapTileLayer) tiledMap.getLayers().get("tileObjectLayer");
		objectLayer = tiledMap.getLayers().get("objectLayer");
	}

	public TiledMap getTiledMap() {
		return tiledMap;
	}

	public TiledMapTileLayer getMainLayer() {
		return mainLayer;
	}

	public TiledMapTileLayer getTileObjectLayer() {
		return tileObjectLayer;
	}

	public MapLayer getObjectLayer() {
		return objectLayer;
	}

	public int getId() {
		return id;
	}

}