package com.realmofwrath.game.custom;

public enum Direction {

	// 3 BITS
	NORTH(0),
	NORTH_EAST(1),
	EAST(2),
	SOUTH_EAST(3),
	SOUTH(4),
	SOUTH_WEST(5),
	WEST(6),
	NORTH_WEST(7);

	private final int id;

	private Direction(int direction) {
		this.id = direction;
	}

	public int getValue() {
		return id;
	}

	public static Direction getDirection(int id) {
		for (Direction direction : values()) {
			if (direction.id == id) {
				return direction;
			}
		}
		return null;
	}

}
