package com.realmofwrath.game.desktop;

import static com.realmofwrath.game.RealmOfWrath.CLIENT_HEIGHT;
import static com.realmofwrath.game.RealmOfWrath.CLIENT_WIDTH;
import static com.realmofwrath.game.RealmOfWrath.TITLE;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.realmofwrath.game.RealmOfWrath;

public class DesktopLauncher {

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = TITLE;
		config.width = CLIENT_WIDTH;
		config.height = CLIENT_HEIGHT;

		//config.fullscreen = true;
				config.vSyncEnabled = false;
		//config.foregroundFPS = 0;


		new LwjglApplication(new RealmOfWrath(), config);
	}

}