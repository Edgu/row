package com.realmofwrath.game.ui;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.realmofwrath.game.RealmOfWrath;
import com.realmofwrath.game.custom.Constants;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.components.SlotComponent;
import com.realmofwrath.game.ecs.entities.Item;
import com.realmofwrath.game.ecs.entities.Player;
import com.realmofwrath.game.ecs.systems.InventorySystem;
import com.realmofwrath.game.inventory.ItemType;
import com.realmofwrath.game.ui.inventory.EquipmentInventorySlot;
import com.realmofwrath.game.ui.inventory.InventorySlot;

public class GameInterface extends Stage {

	private List<InventorySlot> inventorySlots;
	private Player player;
	private InventorySlot draggedSlot;
	private Vector2 cursorPosition;

	// Root tables
	private Table inventoryTable;
	private Table hudTable;

	// Child tables
	private Table equipmentSlotsTable;
	private Table inventorySlotsTable;

	public GameInterface(Player player) {
		super(new StretchViewport(RealmOfWrath.CLIENT_WIDTH, RealmOfWrath.CLIENT_HEIGHT));
		this.player = player;
		cursorPosition = new Vector2(Vector2.Zero);
		setupHUD();
		setupInventoryTable();
	}

	private void setupHUD() {
		Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("gfx/ui/inventory.png"))));
		ImageButtonStyle style = new ImageButton.ImageButtonStyle();
		style.up = drawable;
		style.down = drawable;
		Button inventoryButton = new ImageButton(style);

		inventoryButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				inventoryTable.setVisible(!inventoryTable.isVisible());
				super.clicked(event, x, y);
			}
		});

		hudTable = new Table();
		hudTable.setFillParent(true);
		hudTable.align(Align.topRight);
		hudTable.pad(10);

		hudTable.add(inventoryButton);
		addActor(hudTable);
	}

	private void setupInventoryTable() {
		inventoryTable = new Table();
		inventoryTable.setFillParent(true);
		inventoryTable.setVisible(false);

		equipmentSlotsTable = new Table();
		inventoryTable.add(equipmentSlotsTable).expandX();

		inventorySlotsTable = new Table();
		inventoryTable.add(inventorySlotsTable).expandX().top();

		inventorySlots = new ArrayList<InventorySlot>();

		List<SlotComponent> slots = Mappers.INV_CM.get(player).slots;

		for (int i = 0; i < slots.size(); i++) {
			SlotComponent slotComponent = slots.get(i);

			InventorySlot inventorySlot;

			if (i < Constants.INVENTORY_EQUIPMENT_SLOTS_AMOUNT) {
				if (i != 0 && i % 3 == 0) {
					equipmentSlotsTable.row();
				}
				inventorySlot = new EquipmentInventorySlot(slotComponent, ItemType.getType(i + 1));
				equipmentSlotsTable.add(inventorySlot).pad(5);
			} else {
				int offsetI = i - Constants.INVENTORY_EQUIPMENT_SLOTS_AMOUNT;
				if (offsetI != 0 && offsetI % 6 == 0) {
					inventorySlotsTable.row();
				}
				inventorySlot = new InventorySlot(slotComponent);
				inventorySlotsTable.add(inventorySlot).pad(5);
			}

			inventorySlot.addListener(new ClickListener() {
				@Override
				public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
					if (button == Buttons.LEFT) {
						InventorySlot is = (InventorySlot) event.getTarget();
						if (is.getItem() != null) {
							is.setDragging(true);
							draggedSlot = is;
						}
					}
					return super.touchDown(event, x, y, pointer, button);
				}

				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
					if (button == Buttons.LEFT) {
						InventorySlot is = (InventorySlot) event.getTarget();
						if (draggedSlot == is) {
							is.setDragging(false);
							draggedSlot = null;
						}
						super.touchUp(event, x, y, pointer, button);
					}
				}
			});

			inventorySlots.add(inventorySlot);
		}

		addActor(inventoryTable);
		//		inventoryTable.debugAll();
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (button == Buttons.LEFT) {
			if (draggedSlot != null) {
				cursorPosition.set(screenX, screenY);
				Vector2 coords = getViewport().unproject(cursorPosition);

				Actor actor = hit(coords.x, coords.y, false);

				SWAP_SLOTS: {
					if (actor instanceof InventorySlot) {
						InventorySlot selectedSlot = (InventorySlot) actor;

						if (draggedSlot != selectedSlot) {
							ItemType draggedSlotItemType = Mappers.ITE_CM.get(draggedSlot.getItem()).itemType;

							if (selectedSlot.getItem() == null) {
								if (!(draggedSlot.getType() == selectedSlot.getType() || selectedSlot.getType() == ItemType.DEFAULT || draggedSlotItemType == selectedSlot.getType())) {
									break SWAP_SLOTS;
								}
							} else {
								if (!(draggedSlot.getType() == selectedSlot.getType() || draggedSlotItemType == selectedSlot.getType())) {
									break SWAP_SLOTS;
								}
							}
						}
						
						InventorySystem.swapSlots(player, inventorySlots.indexOf(draggedSlot), inventorySlots.indexOf(actor));
					}
				}
			}
		}
		return super.touchUp(screenX, screenY, pointer, button);
	}

	public void render(float deltaTime) {
		getViewport().apply();
		act();
		draw();

		if (draggedSlot != null) {
			Item draggedSlotItem = draggedSlot.getItem();
			if (draggedSlotItem != null) {
				Sprite sprite = Mappers.SPR_CM.get(draggedSlot.getItem()).sprite;
				if (sprite != null) {
					cursorPosition.set(Gdx.input.getX(), Gdx.input.getY());
					cursorPosition.set(getViewport().unproject(cursorPosition));
					Batch batch = getBatch();
					batch.begin();
					batch.draw(sprite, cursorPosition.x, cursorPosition.y);
					batch.end();
				}
			}
		}

	}

	public void resize(int width, int height) {
		getViewport().update(width, height);
		inventoryTable.validate();
		hudTable.validate();
	}

	private boolean inBounds(int slotId) {
		return slotId >= 0 && slotId < inventorySlots.size();
	}

	public InventorySlot getInventorySlot(int slotId) {
		return inBounds(slotId) ? inventorySlots.get(slotId) : null;
	}

}