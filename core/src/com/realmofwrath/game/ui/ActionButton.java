package com.realmofwrath.game.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ActionButton extends Button {
	
	private Image icon;
	
	public ActionButton(TextureRegion bg, TextureRegion icon) {
		super(new ButtonStyle(new TextureRegionDrawable(bg), new TextureRegionDrawable(bg), new TextureRegionDrawable(bg)));
		this.icon = new Image(icon);
		this.icon.setOrigin(this.icon.getWidth() / 2, this.icon.getHeight() / 2);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		icon.setPosition(getX() + getWidth() / 2 - icon.getWidth() / 2, getY() + getHeight() / 2 - icon.getHeight() / 2);
		icon.draw(batch, parentAlpha);
	}
}