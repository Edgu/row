package com.realmofwrath.game.ui.inventory;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.realmofwrath.game.ecs.components.SlotComponent;
import com.realmofwrath.game.inventory.ItemType;
import com.realmofwrath.game.managers.TextureManager;

public class EquipmentInventorySlot extends InventorySlot {

	private Texture texture;

	public EquipmentInventorySlot(SlotComponent slotComponent, ItemType itemType) {
		super(slotComponent);
		type = itemType;

		switch (type) {
		case ARMOUR:
			texture = TextureManager.ARMOUR;
			break;
		case BACKPACK:
			texture = TextureManager.BACKPACK;
			break;
		case BELT:
			texture = TextureManager.BELT;
			break;
		case BOOTS:
			texture = TextureManager.BOOTS;
			break;
		case GLOVES:
			texture = TextureManager.GLOVES;
			break;
		case HELMET:
			texture = TextureManager.HELMET;
			break;
		case NECKLACE:
			texture = TextureManager.NECKLACE;
			break;
		case BRACELET:
			texture = TextureManager.BRACELET;
			break;
		case PANTS:
			texture = TextureManager.PANTS;
			break;
		case RING:
			texture = TextureManager.RING;
			break;
		case SHIELD:
			texture = TextureManager.SHIELD;
			break;
		case WEAPON:
			texture = TextureManager.WEAPON;
			break;
		default:
			System.err.println(getClass().getSimpleName());
			System.exit(0);
			break;
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		if (isBeingDragged() || getItem() == null) {
			batch.draw(texture, getX() + ((getWidth() / 2) - (texture.getWidth() / 2)), getY() + ((getHeight() / 2) - (texture.getHeight() / 2)));
		}
	}

}