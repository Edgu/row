package com.realmofwrath.game.ui.inventory;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.components.SlotComponent;
import com.realmofwrath.game.ecs.entities.Item;
import com.realmofwrath.game.inventory.ItemType;
import com.realmofwrath.game.managers.TextureManager;

public class InventorySlot extends Button {

	protected ItemType type;
	private SlotComponent slotComponent;
	private boolean dragging;

	public InventorySlot(SlotComponent slotComponent) {
		this.slotComponent = slotComponent;
		type = ItemType.DEFAULT;
		ButtonStyle style = new ButtonStyle();
		style.up = TextureManager.inventorySlot;
		super.setStyle(style);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, 1);

		if (!dragging && slotComponent.item != null) {
			Sprite itemSprite = Mappers.SPR_CM.get(slotComponent.item).sprite;
			batch.draw(itemSprite, getX() + ((getWidth() / 2) - (itemSprite.getWidth() / 2)), getY() + ((getHeight() / 2) - (itemSprite.getHeight() / 2)));
		}
	}

	public Item getItem() {
		return slotComponent.item;
	}

	public ItemType getType() {
		return type;
	}

	public boolean isBeingDragged() {
		return dragging;
	}

	public void setDragging(boolean dragging) {
		this.dragging = dragging;
	}

}