package com.realmofwrath.game.screens;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.realmofwrath.game.ClientMap;
import com.realmofwrath.game.RealmOfWrath;
import com.realmofwrath.game.custom.Node;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Util;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.SpriteComponent;
import com.realmofwrath.game.ecs.entities.Player;
import com.realmofwrath.game.ecs.systems.RenderSystem;
import com.realmofwrath.game.managers.EntityManager;
import com.realmofwrath.game.network.packet.PacketRequestMapData;
import com.realmofwrath.game.network.packet.inventory.PacketRequestInventory;
import com.realmofwrath.game.network.packet.select.PacketSelectNPC;
import com.realmofwrath.game.network.packet.select.PacketSelectPlayer;
import com.realmofwrath.game.network.packet.select.PacketSelectSkillTile;
import com.realmofwrath.game.network.packet.select.PacketSelectTile;
import com.realmofwrath.game.ui.GameInterface;

public class GameScreen implements Screen, InputProcessor {

	private final EntityManager entityManager;
	private SpriteBatch spriteBatch;
	private InputMultiplexer inputMultiplexer;
	private OrthogonalTiledMapRenderer tiledMapRenderer;
	private static OrthographicCamera camera;
	private PositionComponent pc;
	private final GameInterface gameInterface;

	private final int TICKS_PER_SECOND = 60;
	private final float STEP = 1f / TICKS_PER_SECOND;
	private float accumulator;

	private final Vector2Integer cameraOffset;

	private RenderSystem renderSystem;

	private float zoom;

	// FIXME Use map that player is in.
	public static ClientMap clientMap;
	public static List<ClientMap> maps;
	private int currentMapId = -1;

	// TEST
	private SpriteBatch HUD;
	private BitmapFont font;

	private Viewport viewport;

	public GameScreen(final EntityManager entityManager, SpriteBatch spriteBatch) {
		this.entityManager = entityManager;
		this.spriteBatch = spriteBatch;

		// Load maps
		TmxMapLoader.Parameters parameters = new TmxMapLoader.Parameters();
		parameters.textureMinFilter = TextureFilter.Nearest;
		parameters.textureMagFilter = TextureFilter.Nearest;

		maps = new ArrayList<ClientMap>();

		for (int i = 0; i < 2; i++) {
			maps.add(new ClientMap(i, new TmxMapLoader().load("maps/" + i + ".tmx", parameters)));
		}

		clientMap = maps.get(Mappers.MAP_CM.get(entityManager.getPlayer()).mapId);

		tiledMapRenderer = new OrthogonalTiledMapRenderer(clientMap.getTiledMap());
		camera = new OrthographicCamera();
		viewport = new ScreenViewport(camera);
		pc = Mappers.POS_CM.get(entityManager.getPlayer());
		gameInterface = new GameInterface(entityManager.getPlayer());
		inputMultiplexer = new InputMultiplexer();

		inputMultiplexer.addProcessor(gameInterface);
		inputMultiplexer.addProcessor(this);
		Gdx.input.setInputProcessor(inputMultiplexer);

		SpriteComponent spriteComponent = Mappers.SPR_CM.get(entityManager.getPlayer());
		cameraOffset = new Vector2Integer((int) spriteComponent.sprite.getWidth() >> 1, (int) spriteComponent.sprite.getHeight() >> 1);

		renderSystem = entityManager.getSystem(RenderSystem.class);
		font = new BitmapFont();
		font.getData().setScale(3);
		HUD = new SpriteBatch();

		RealmOfWrath.gameClient.getKryonetClient().sendTCP(new PacketRequestInventory());
	}

	@Override
	public void show() {

	}

	private void changeMap() {
		System.out.println("Change map");

		Player player = entityManager.getPlayer();
		entityManager.removeAllEntities();
		entityManager.addEntity(player);

		clientMap = maps.get(Mappers.MAP_CM.get(entityManager.getPlayer()).mapId);

		clientMap.resetTileObjectLayer();

		tiledMapRenderer.setMap(clientMap.getTiledMap());

		RealmOfWrath.gameClient.getKryonetClient().sendTCP(new PacketRequestMapData());
	}

	@Override
	public void render(float deltaTime) {
		viewport.apply();
		accumulator += deltaTime;

		if (currentMapId != clientMap.getId()) {
			changeMap();
		}

		currentMapId = Mappers.MAP_CM.get(entityManager.getPlayer()).mapId;

		spriteBatch.setProjectionMatrix(camera.combined);

		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();

		spriteBatch.begin();
		renderSystem.update(deltaTime);
		spriteBatch.end();

		while (accumulator >= STEP) {
			entityManager.update(1);
			accumulator -= STEP;
		}

		camera.position.x = pc.position.x + cameraOffset.x;
		camera.position.y = pc.position.y + cameraOffset.y;
		camera.update();

		HUD.begin();
		font.draw(HUD, "RTT: " + RealmOfWrath.gameClient.getKryonetClient().getReturnTripTime(), 10, 45);
		font.draw(HUD, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, 95);
		HUD.end();

		gameInterface.render(STEP);
		RealmOfWrath.getTweenManager().update(STEP);
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
		float scaleX = viewport.getWorldWidth() / RealmOfWrath.GAME_WORLD_SIZE.x;
		float scaleY = viewport.getWorldHeight() / RealmOfWrath.GAME_WORLD_SIZE.y;
		zoom = 1.0f / Math.min(scaleX, scaleY);
		camera.zoom = zoom;
		gameInterface.resize(width, height);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {
//		changeMap();
	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		tiledMapRenderer.dispose();
	}

	public static OrthographicCamera getCamera() {
		return camera;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	private Entity getClosestEntity(Vector2Integer position, ImmutableArray<Entity> entities) {
		Entity closestEntity = null;
		int closestDistance = Integer.MAX_VALUE;

		Sprite playerSprite = Mappers.SPR_CM.get(entityManager.getPlayer()).sprite;

		for (Entity entity : entities) {
			PositionComponent one = Mappers.POS_CM.get(entity);
			int distance = Vector2Integer.chebyshevDistance(position, new Vector2Integer(one.position.x + ((int) playerSprite.getWidth() / 2), one.position.y + ((int) playerSprite.getHeight() / 2)));
			if (distance <= 16) { // TODO distance depends on player size. x, y
				if (closestDistance > distance) {
					closestEntity = entity;
					closestDistance = distance;
					if (distance == 0) {
						break;
					}
				}
			}
		}

		return closestEntity;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Entity player = entityManager.getPlayer();
		if (Mappers.HP_CM.get(player).health > 0) {
			Vector2Integer mapPosition = new Vector2Integer(camera.unproject(new Vector3(screenX, screenY, 0)));
			Vector2Integer mapTilePosition= TiledUtils.positionToTiled(mapPosition);
			Vector2Integer playerCurrentMove = Mappers.MQU_CM.get(player).currentMove;
			Vector2Integer playerPosition = (playerCurrentMove == null) ? TiledUtils.positionToTiled(Mappers.POS_CM.get(player).position) : playerCurrentMove;

			TiledMapTile tile = TiledUtils.getTile(clientMap.getMainLayer(), mapTilePosition);
			TiledMapTile objectTile = TiledUtils.getTile(clientMap.getTileObjectLayer(), mapTilePosition);

			Entity entity = null;

			// FIXME Touchdown - check if reachable or can see before sending packet.
			if ((entity = getClosestEntity(mapPosition, entityManager.getPlayers())) != null) {
				if (entity != entityManager.getPlayer()) {
					PacketSelectPlayer packet = new PacketSelectPlayer();
					packet.id = Mappers.PLA_CM.get(entity).id;
					System.out.println(packet.id);
					RealmOfWrath.gameClient.getKryonetClient().sendTCP(packet);
					System.out.println("Send: " + packet.getClass().getSimpleName());
				}
			} else if ((entity = getClosestEntity(mapPosition, entityManager.getNpcs())) != null) {
				PacketSelectNPC packet = new PacketSelectNPC();
				packet.instanceId = Mappers.NPC_CM.get(entity).instanceId;
				RealmOfWrath.gameClient.getKryonetClient().sendTCP(packet);
			} else if (objectTile != null && objectTile.getProperties().containsKey("resource")) {
				List<Node> path = Util.findPath(clientMap, playerPosition, mapTilePosition, false, true);
				if (!path.isEmpty()) {
					int index = (path.size() == 1) ? 0 : 1;
					Vector2Integer mapTilePositionBesideResource = path.get(index).position;
					PacketSelectSkillTile packet = new PacketSelectSkillTile();
					packet.x = mapTilePosition.x;
					packet.y = mapTilePosition.y;
					packet.tX = mapTilePositionBesideResource.x;
					packet.tY = mapTilePositionBesideResource.y;
					RealmOfWrath.gameClient.getKryonetClient().sendTCP(packet);
				} else {
					System.err.println("OBJECT PATH IS NOT FOUND!");
				}
			} else if (tile != null && !TiledUtils.isTileSolid(tile) && objectTile == null) {
				if (Vector2Integer.chebyshevDistance(mapTilePosition, playerPosition) > 0) {
					List<Node> path = Util.findPath(clientMap, mapTilePosition, playerPosition, false, false);
					if (!path.isEmpty()) {
						PacketSelectTile packet = new PacketSelectTile();
						packet.x = mapTilePosition.x;
						packet.y = mapTilePosition.y;
						RealmOfWrath.gameClient.getKryonetClient().sendTCP(packet);
					} else {
						System.err.println("PATH IS NOT FOUND!");
					}
				} else {
					System.err.println("ALREADY ON THIS TILE");
				}
			}
		} else {
			System.err.println("YOU CAN NOT MOVE SINCE YOU ARE DEAD!");
		}

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}