package com.realmofwrath.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.ecs.components.AttributeComponent;
import com.realmofwrath.game.ecs.entities.Item;
import com.realmofwrath.game.inventory.Attribute;
import com.realmofwrath.game.inventory.ItemType;
import com.realmofwrath.game.managers.EntityManager;
import com.realmofwrath.game.managers.TextureManager;
import com.realmofwrath.game.network.GameClient;
import com.realmofwrath.game.screens.GameScreen;
import com.realmofwrath.game.tween.OrthographicCameraAccessor;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

public class RealmOfWrath extends Game {

	public static final String TITLE = "Realm of Wrath";
//			public static final String SERVER_ADDRESS = "server.edgu.lt";
	public static final String SERVER_ADDRESS = "192.168.1.5";
	public static final int SERVER_PORT = 27015;
	public static final Vector2Integer GAME_WORLD_SIZE = new Vector2Integer(400, 240);
	public static final int CLIENT_WIDTH = GAME_WORLD_SIZE.x * 2;
	public static final int CLIENT_HEIGHT = GAME_WORLD_SIZE.y * 2;

	public static GameClient gameClient;
	private EntityManager entityManager;
	private SpriteBatch spriteBatch;
	private static TweenManager tweenManager;

	private static Item[] items;

	public RealmOfWrath() {
		System.out.println("ROW Constructor.");
	}

	@Override
	public void create() {
		JsonReader jsonReader = new JsonReader();

		// Load Item attributes
		JsonValue itemAttributeValues = jsonReader.parse(Gdx.files.internal("data/item_attribute.json"));

		Map<Integer, List<AttributeComponent>> itemAttributes = new HashMap<Integer, List<AttributeComponent>>();

		for (JsonValue jsonValue : itemAttributeValues) {
			int itemId = jsonValue.getInt("item_id");

			if (!itemAttributes.containsKey(itemId)) {
				itemAttributes.put(itemId, new ArrayList<AttributeComponent>());
			}

			itemAttributes.get(itemId).add(new AttributeComponent(Attribute.getAttribute(jsonValue.getInt("attribute_id")), jsonValue.getInt("value")));
		}

		// Load items
		JsonValue itemValues = jsonReader.parse(Gdx.files.internal("data/item.json"));

		items = new Item[itemValues.size];

		for (int i = 0; i < itemValues.size; i++) {
			JsonValue jsonValue = itemValues.get(i);

			ItemType type = ItemType.getType(jsonValue.getInt("type"));
			String name = jsonValue.getString("name");

			// FIXME spritesheet
			Sprite sprite = new Sprite(new Texture(Gdx.files.internal("gfx/item/" + i + ".png")));

			List<AttributeComponent> attributes = itemAttributes.get(i);

			items[i] = new Item(i, name, type, (attributes == null) ? null : attributes.toArray(new AttributeComponent[attributes.size()]), sprite);
		}

		// Load npc sprites
		itemValues = jsonReader.parse(Gdx.files.internal("npc/npc.json"));
		TextureManager.npcSprites = new Sprite[itemValues.size];

		for (int i = 0; i < itemValues.size; i++) {
			TextureManager.npcSprites[i] = new Sprite(new Texture(Gdx.files.internal("npc/" + i + ".png")));
		}

		TiledUtils.init();

		spriteBatch = new SpriteBatch();
		entityManager = new EntityManager(spriteBatch);
		tweenManager = new TweenManager();
		Tween.registerAccessor(OrthographicCamera.class, new OrthographicCameraAccessor());

		gameClient = new GameClient(entityManager);
		gameClient.connect();

		setScreen(new GameScreen(entityManager, spriteBatch));
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0.317647059f, 0.30588235294f, 0.30588235294f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		super.render();
	}

	@Override
	public void dispose() {
		super.dispose();
		gameClient.getKryonetClient().close();
	}

	public static TweenManager getTweenManager() {
		return tweenManager;
	}

	public static Item getItem(int id) {
		return (id >= 0 && id < items.length) ? items[id] : null;
	}

}