package com.realmofwrath.game.tween;

import com.badlogic.gdx.graphics.OrthographicCamera;

import aurelienribon.tweenengine.TweenAccessor;

public class OrthographicCameraAccessor implements TweenAccessor<OrthographicCamera> {
	
	public static final int POSITION = 1;

	@Override
	public int getValues(OrthographicCamera target, int tweenType, float[] returnValues) {
		switch (tweenType) {
			case POSITION:
				returnValues[0] = target.position.x;
				returnValues[1] = target.position.y;
				return 2;
		}
		return 0;
	}

	@Override
	public void setValues(OrthographicCamera target, int tweenType, float[] newValues) {
		switch (tweenType) {
			case POSITION:
				target.position.x = newValues[0];
				target.position.y = newValues[1];
				target.update();
				break;
		}
	}

}