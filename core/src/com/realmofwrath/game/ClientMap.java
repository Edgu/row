package com.realmofwrath.game;

import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;

public class ClientMap extends Map {

	private java.util.Map<Vector2Integer, StaticTiledMapTile> tol;

	public ClientMap(int id, TiledMap tiledMap) {
		super(id, tiledMap);

		tol = new HashMap<Vector2Integer, StaticTiledMapTile>();

		TiledMapTileLayer layer = getTileObjectLayer();

		for (int x = 0; x < layer.getWidth(); x++) {
			for (int y = 0; y < layer.getHeight(); y++) {
				Cell cell = layer.getCell(x, y);
				if (cell != null) {
					TiledMapTile tile = cell.getTile();
					if (tile.getProperties().containsKey("resource")) {
						Vector2Integer position = new Vector2Integer(x, y);
						switch (tile.getProperties().get("name").toString()) {
						case "tree":
							tol.put(position, TiledUtils.TREE);
							break;
						case "rock":
							tol.put(position, TiledUtils.ROCK);
							break;
						case "fish":
							tol.put(position, TiledUtils.FISH);
							break;
						default:
							System.err.println("UNHANDLED RESOURCE: CLIENTMAP");
							break;
						}
					}
				}
			}
		}

	}

	public void resetTileObjectLayer() {
		TiledMapTileLayer layer = getTileObjectLayer();
		for (Entry<Vector2Integer, StaticTiledMapTile> set : tol.entrySet()) {
			TiledUtils.setTile(layer, set.getValue(), set.getKey());
		}
	}

}