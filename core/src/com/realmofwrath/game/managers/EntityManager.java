package com.realmofwrath.game.managers;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.components.DropBagComponent;
import com.realmofwrath.game.ecs.components.IdComponent;
import com.realmofwrath.game.ecs.components.NPCComponent;
import com.realmofwrath.game.ecs.components.PlayerComponent;
import com.realmofwrath.game.ecs.entities.DropBag;
import com.realmofwrath.game.ecs.entities.NPC;
import com.realmofwrath.game.ecs.entities.Player;
import com.realmofwrath.game.ecs.systems.MovementSystem;
import com.realmofwrath.game.ecs.systems.RenderSystem;

public class EntityManager extends Engine {

	private Player player;
	
	public EntityManager(SpriteBatch spriteBatch) {
		addSystem(new RenderSystem(spriteBatch, 0));
		addSystem(new MovementSystem());
		player = new Player();
		addEntity(player);
	}

	public ImmutableArray<Entity> getPlayers() {
		Family family = Family.all(PlayerComponent.class).get();
		return getEntitiesFor(family);
	}

	public ImmutableArray<Entity> getDrops() {
		Family family = Family.all(DropBagComponent.class).get();
		return getEntitiesFor(family);
	}

	public Entity getPlayer(int id) {
		ImmutableArray<Entity> entities = getPlayers();
		for (int i = 0; i < entities.size(); i++) {
			Entity player = entities.get(i);
			PlayerComponent playerComponent = Mappers.PLA_CM.get(player);
			if (playerComponent.id == id) {
				return player;
			}
		}
		return null;
	}

	public Player getPlayer() {
		return player;
	}

	public ImmutableArray<Entity> getNpcs() {
		Family family = Family.all(NPCComponent.class).get();
		return getEntitiesFor(family);
	}

	public Entity getNPC(int instanceId) {
		ImmutableArray<Entity> entities = getNpcs();
		for (int i = 0; i < entities.size(); i++) {
			Entity entity = entities.get(i);
			NPCComponent npcComponent = Mappers.NPC_CM.get(entity);
			if (npcComponent != null && npcComponent.instanceId == instanceId) {
				return entity;
			}
		}
		return null;
	}

	public DropBag getDrop(int id) {
		ImmutableArray<Entity> entities = getDrops();
		for (int i = 0; i < entities.size(); i++) {
			DropBag drop = (DropBag) entities.get(i);
			IdComponent idComponent = Mappers.ID_CM.get(drop);
			if (idComponent.id == id) {
				return drop;
			}
		}
		return null;
	}

	public NPC getNpcOnTile(Vector2Integer position) {
		ImmutableArray<Entity> entities = getNpcs();
		for (Entity entity : entities) {
			if (TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position).equals(position)) {
				return (NPC) entity;
			}
		}
		return null;
	}

	public DropBag getBagOnTile(Vector2Integer position) {
		ImmutableArray<Entity> entities = getDrops();
		for (Entity entity : entities) {
			if (TiledUtils.positionToTiled(Mappers.POS_CM.get(entity).position).equals(position)) {
				return (DropBag) entity;
			}
		}
		return null;
	}

}