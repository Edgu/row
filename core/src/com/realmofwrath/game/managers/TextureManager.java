package com.realmofwrath.game.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class TextureManager {

	public static Sprite[] npcSprites;

	public static Sprite playerSprite;

	public static final Texture NECKLACE;
	public static final Texture HELMET;
	public static final Texture BACKPACK;
	public static final Texture WEAPON;
	public static final Texture ARMOUR;
	public static final Texture SHIELD;
	public static final Texture BRACELET;
	public static final Texture PANTS;
	public static final Texture RING;
	public static final Texture GLOVES;
	public static final Texture BELT;
	public static final Texture BOOTS;

	public static TextureRegionDrawable inventorySlot;

	static {
		inventorySlot = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("gfx/inventory/inventorySlot.png"))));

		NECKLACE = new Texture(Gdx.files.internal("gfx/inventory/necklace.png"));
		HELMET = new Texture(Gdx.files.internal("gfx/inventory/helmet.png"));
		BACKPACK = new Texture(Gdx.files.internal("gfx/inventory/backpack.png"));
		WEAPON = new Texture(Gdx.files.internal("gfx/inventory/weapon.png"));
		ARMOUR = new Texture(Gdx.files.internal("gfx/inventory/armour.png"));
		SHIELD = new Texture(Gdx.files.internal("gfx/inventory/shield.png"));
		BRACELET = new Texture(Gdx.files.internal("gfx/inventory/bracelet.png"));
		PANTS = new Texture(Gdx.files.internal("gfx/inventory/pants.png"));
		RING = new Texture(Gdx.files.internal("gfx/inventory/ring.png"));
		GLOVES = new Texture(Gdx.files.internal("gfx/inventory/gloves.png"));
		BELT = new Texture(Gdx.files.internal("gfx/inventory/belt.png"));
		BOOTS = new Texture(Gdx.files.internal("gfx/inventory/boots.png"));

		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("test/player.txt"));
		playerSprite = atlas.createSprite("char");
	}

}