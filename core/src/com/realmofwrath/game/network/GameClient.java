package com.realmofwrath.game.network;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage.Ping;
import com.esotericsoftware.kryonet.Listener;
import com.realmofwrath.game.RealmOfWrath;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Util;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.entities.Item;
import com.realmofwrath.game.ecs.entities.MPPlayer;
import com.realmofwrath.game.ecs.entities.NPC;
import com.realmofwrath.game.ecs.entities.Player;
import com.realmofwrath.game.ecs.systems.InventorySystem;
import com.realmofwrath.game.managers.EntityManager;
import com.realmofwrath.game.network.packet.PacketMove;
import com.realmofwrath.game.network.packet.PacketNPCKill;
import com.realmofwrath.game.network.packet.PacketRequestMapData;
import com.realmofwrath.game.network.packet.inventory.PacketInventoryData;
import com.realmofwrath.game.network.packet.inventory.PacketInventorySlotRemove;
import com.realmofwrath.game.network.packet.inventory.PacketInventorySlotUpdate;
import com.realmofwrath.game.network.packet.inventory.PacketRequestInventory;
import com.realmofwrath.game.network.packet.npc.PacketNPCSpawn;
import com.realmofwrath.game.network.packet.player.PacketClientData;
import com.realmofwrath.game.network.packet.player.PacketNewPlayer;
import com.realmofwrath.game.network.packet.player.PacketRemovePlayer;
import com.realmofwrath.game.network.packet.select.PacketSelectNPC;
import com.realmofwrath.game.network.packet.select.PacketSelectPlayer;
import com.realmofwrath.game.network.packet.select.PacketSelectSkillTile;
import com.realmofwrath.game.network.packet.select.PacketSelectTile;
import com.realmofwrath.game.network.packet.skill.fishing.PacketFishDepleted;
import com.realmofwrath.game.network.packet.skill.fishing.PacketFishReplenish;
import com.realmofwrath.game.network.packet.skill.mining.PacketRockMined;
import com.realmofwrath.game.network.packet.skill.mining.PacketRockReplenish;
import com.realmofwrath.game.network.packet.skill.woodcutting.PacketTreeCutDown;
import com.realmofwrath.game.network.packet.skill.woodcutting.PacketTreeRegrow;
import com.realmofwrath.game.screens.GameScreen;

public class GameClient extends Listener {

	private Client client;
	private EntityManager entityManager;

	public GameClient(EntityManager entityManager) {
		System.out.println("Client created.");
		this.entityManager = entityManager;
		this.client = new Client();
		client.addListener(this);
		registerPackets();
	}

	private void registerPackets() {
		Kryo kryo = client.getKryo();
		kryo.register(PacketInventorySlotUpdate.class);
		kryo.register(PacketInventorySlotRemove.class);
		kryo.register(PacketNPCSpawn.class);
		kryo.register(PacketClientData.class);
		kryo.register(PacketNewPlayer.class);
		kryo.register(PacketRemovePlayer.class);
		kryo.register(PacketSelectNPC.class);
		kryo.register(PacketSelectPlayer.class);
		kryo.register(PacketSelectSkillTile.class);
		kryo.register(PacketSelectTile.class);

		kryo.register(PacketRockMined.class);
		kryo.register(PacketRockReplenish.class);

		kryo.register(PacketTreeCutDown.class);
		kryo.register(PacketTreeRegrow.class);

		kryo.register(PacketMove.class);
		kryo.register(PacketNPCKill.class);
		kryo.register(PacketRequestMapData.class);

		kryo.register(PacketFishDepleted.class);
		kryo.register(PacketFishReplenish.class);

		kryo.register(PacketRequestInventory.class);
		kryo.register(PacketInventoryData.class);

		kryo.register(int[].class);
	}

	public void connect() {
		//		client.setKeepAliveTCP(10000);
		new Thread(client).start();
		try {
			client.connect(5000, RealmOfWrath.SERVER_ADDRESS, RealmOfWrath.SERVER_PORT, RealmOfWrath.SERVER_PORT);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void connected(Connection connection) {
		System.out.println("Client connected.");
		client.updateReturnTripTime();
	}

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof Ping) {
//						client.updateReturnTripTime();
			return;
		}

		client.updateReturnTripTime();
		if (object instanceof PacketClientData) {
			PacketClientData packet = (PacketClientData) object;
			System.out.println(packet.mapId);
			Player player = entityManager.getPlayer();
			Mappers.PLA_CM.get(player).id = packet.id;
			Mappers.NAM_CM.get(player).name = packet.name;
			Mappers.MAP_CM.get(player).mapId = packet.mapId;
			Mappers.POS_CM.get(player).position = TiledUtils.positionToMap(packet.x, packet.y);
		} else if (object instanceof PacketNewPlayer) {
			PacketNewPlayer packet = (PacketNewPlayer) object;
			System.out.println("NEW PLAYER: " + packet.id);
			entityManager.addEntity(new MPPlayer(packet.id, TiledUtils.positionToMap(packet.x, packet.y)));
		} else if (object instanceof PacketRemovePlayer) {
			PacketRemovePlayer packet = (PacketRemovePlayer) object;
			Entity entity = entityManager.getPlayer(packet.id);
			if (entity != null) {
				entityManager.removeEntity(entity);
			}
		} else if (object instanceof PacketMove) {
			PacketMove packet = (PacketMove) object;
			Entity entity = (packet.type == 1) ? entityManager.getPlayer(packet.id) : entityManager.getNPC(packet.id);
			if (entity != null) {
				Mappers.MQU_CM.get(entity).queue.add(Util.directionToMove(packet.direction));
			}
		} else if (object instanceof PacketNPCSpawn) {
			PacketNPCSpawn packet = (PacketNPCSpawn) object;
			Vector2Integer position = TiledUtils.positionToMap(packet.x, packet.y);
			entityManager.addEntity(new NPC(packet.instanceId, packet.id, position.x, position.y, packet.health, packet.maxHealth));
		} else if (object instanceof PacketNPCKill) {
			PacketNPCKill packet = (PacketNPCKill) object;
			Entity entity = entityManager.getNPC(packet.instanceId);
			if (entity != null) {
				entityManager.removeEntity(entity);
			}
		} else if (object instanceof PacketTreeCutDown) {
			PacketTreeCutDown packet = (PacketTreeCutDown) object;
			TiledUtils.setTile(GameScreen.clientMap.getTileObjectLayer(), TiledUtils.TREE_STUMP, packet.x, packet.y);
		} else if (object instanceof PacketTreeRegrow) {
			PacketTreeRegrow packet = (PacketTreeRegrow) object;
			TiledUtils.setTile(GameScreen.clientMap.getTileObjectLayer(), TiledUtils.TREE, packet.x, packet.y);
		} else if (object instanceof PacketRockMined) {
			PacketRockMined packet = (PacketRockMined) object;
			TiledUtils.setTile(GameScreen.clientMap.getTileObjectLayer(), TiledUtils.ROCK_MINED, packet.x, packet.y);
		} else if (object instanceof PacketRockReplenish) {
			PacketRockReplenish packet = (PacketRockReplenish) object;
			TiledUtils.setTile(GameScreen.clientMap.getTileObjectLayer(), TiledUtils.ROCK, packet.x, packet.y);
		} else if (object instanceof PacketFishDepleted) {
			PacketFishDepleted packet = (PacketFishDepleted) object;
			TiledUtils.setTile(GameScreen.clientMap.getTileObjectLayer(), TiledUtils.FISH_DEPLETED, packet.x, packet.y);
		} else if (object instanceof PacketFishReplenish) {
			PacketFishReplenish packet = (PacketFishReplenish) object;
			TiledUtils.setTile(GameScreen.clientMap.getTileObjectLayer(), TiledUtils.FISH, packet.x, packet.y);
		} else if (object instanceof PacketInventoryData) {
			PacketInventoryData packet = (PacketInventoryData) object;
			System.out.println("[LoadInventory]");
			for (int i = 0; i < packet.items.length; i++) {
				Item item = RealmOfWrath.getItem(packet.items[i]);
				System.out.println(i + ". " + (item == null ? item : Mappers.NAM_CM.get(item).name));
				InventorySystem.setItem(entityManager.getPlayer(), i, item);
			}
		}
	}

	@Override
	public void disconnected(Connection connection) {
		System.out.println("Client connected.");
	}

	public Client getKryonetClient() {
		return client;
	}

}