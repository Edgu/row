package com.realmofwrath.game.ecs.components;

import com.badlogic.ashley.core.Component;

public class PlayerComponent implements Component {

	public int id;
	
	public PlayerComponent(int id) {
		this.id = id;
	}
	
}