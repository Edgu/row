package com.realmofwrath.game.ecs.components;

import com.badlogic.ashley.core.Component;

public class WrathComponent implements Component {
	
	public int wrath;
	public int maxWrath;
	
	public WrathComponent(int wrath, int maxWrath) {
		this.wrath = wrath;
		this.maxWrath = maxWrath;
	}

}