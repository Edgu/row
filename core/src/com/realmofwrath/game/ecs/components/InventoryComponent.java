package com.realmofwrath.game.ecs.components;

import java.util.List;

import com.badlogic.ashley.core.Component;

public class InventoryComponent implements Component {

	public List<SlotComponent> slots;

	public InventoryComponent(List<SlotComponent> slots) {
		this.slots = slots;
	}

}