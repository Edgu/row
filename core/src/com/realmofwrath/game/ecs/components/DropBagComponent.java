package com.realmofwrath.game.ecs.components;

import com.badlogic.ashley.core.Component;

public class DropBagComponent implements Component {

	public int ownerId;
	public SlotComponent[] slots;

	public DropBagComponent(int ownerId, SlotComponent[] slots) {
		this.ownerId = ownerId;
		this.slots = slots;
	}

}