package com.realmofwrath.game.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.ecs.State;

public class StateComponent implements Component {

	public State state;
	
	public StateComponent(State state) {
		this.state = state;
	}

}