package com.realmofwrath.game.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.ecs.entities.Item;

public class SlotComponent implements Component {

	public Item item;
	
	public SlotComponent() {
		this(null);
	}

	public SlotComponent(Item item) {
		this.item = item;
	}

}