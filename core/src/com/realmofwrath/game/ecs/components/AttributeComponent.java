package com.realmofwrath.game.ecs.components;

import com.badlogic.ashley.core.Component;
import com.realmofwrath.game.inventory.Attribute;

public class AttributeComponent implements Component {

	public final Attribute attribute;
	public final int value;

	public AttributeComponent(Attribute attribute, int value) {
		this.attribute = attribute;
		this.value = value;
	}

}