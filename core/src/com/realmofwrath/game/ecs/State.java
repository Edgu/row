package com.realmofwrath.game.ecs;

public enum State {

	IDLE(0),
	WALKING(1),
	WOODCUTTING(2);

	private int stateId;

	private State(int stateId) {
		this.stateId = stateId;
	}

	public int getStateId() {
		return stateId;
	}

	public State getState(int stateId) {
		for (State state : values()) {
			if (state.stateId == stateId) {
				return state;
			}
		}
		return null;
	}

}