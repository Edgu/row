package com.realmofwrath.game.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.ecs.components.HealthComponent;
import com.realmofwrath.game.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.ecs.components.PlayerComponent;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.RenderableComponent;
import com.realmofwrath.game.ecs.components.SpriteComponent;
import com.realmofwrath.game.ecs.components.VelocityComponent;
import com.realmofwrath.game.managers.TextureManager;

public class MPPlayer extends Entity {

	public MPPlayer(int id, Vector2Integer position) {
		add(new PlayerComponent(id));
		add(new PositionComponent(position.x, position.y));
		add(new VelocityComponent());
		add(new SpriteComponent(TextureManager.playerSprite));
		add(new RenderableComponent());
		add(new HealthComponent(100, 100));
		add(new MovementQueueComponent());
	}

}