package com.realmofwrath.game.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.ecs.components.HealthComponent;
import com.realmofwrath.game.ecs.components.InventoryComponent;
import com.realmofwrath.game.ecs.components.MapComponent;
import com.realmofwrath.game.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.ecs.components.PlayerComponent;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.RenderableComponent;
import com.realmofwrath.game.ecs.components.SpriteComponent;
import com.realmofwrath.game.ecs.components.VelocityComponent;
import com.realmofwrath.game.ecs.components.WrathComponent;
import com.realmofwrath.game.ecs.systems.InventorySystem;
import com.realmofwrath.game.managers.TextureManager;

public class Player extends Entity {

	public Player() {
		add(new PlayerComponent(-1));
		add(new com.realmofwrath.game.ecs.components.NameComponent());
		add(new PositionComponent(32, 32));
		add(new VelocityComponent());
		add(new SpriteComponent(TextureManager.playerSprite));
		add(new RenderableComponent());
		add(new HealthComponent(42, 100));
		add(new WrathComponent(0, 100));
		add(new MovementQueueComponent());
		add(new InventoryComponent(InventorySystem.createPlayerInventory()));
		add(new MapComponent());
	}

}