package com.realmofwrath.game.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.realmofwrath.game.ecs.components.AttributeComponent;
import com.realmofwrath.game.ecs.components.IdComponent;
import com.realmofwrath.game.ecs.components.ItemComponent;
import com.realmofwrath.game.ecs.components.NameComponent;
import com.realmofwrath.game.ecs.components.SpriteComponent;
import com.realmofwrath.game.inventory.ItemType;

public class Item extends Entity {

	public Item(int id, String name, ItemType itemType, AttributeComponent[] attributes, Sprite sprite) {
		add(new IdComponent(id));
		add(new NameComponent(name));
		add(new ItemComponent(itemType, attributes));
		add(new SpriteComponent(sprite));
	}

}