package com.realmofwrath.game.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.ecs.components.HealthComponent;
import com.realmofwrath.game.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.ecs.components.NPCComponent;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.RenderableComponent;
import com.realmofwrath.game.ecs.components.SpriteComponent;
import com.realmofwrath.game.ecs.components.VelocityComponent;
import com.realmofwrath.game.managers.TextureManager;

public class NPC extends Entity {

	public NPC(int instanceId, int id, int x, int y, int health, int maxHealth) {
		add(new NPCComponent(id, instanceId));
		add(new PositionComponent(x, y));
		add(new VelocityComponent());
		add(new MovementQueueComponent());
		add(new HealthComponent(health, maxHealth));
		add(new SpriteComponent(TextureManager.npcSprites[id]));
		add(new RenderableComponent());
	}
	
}