package com.realmofwrath.game.ecs.entities;

import com.badlogic.ashley.core.Entity;
import com.realmofwrath.game.ecs.components.DropBagComponent;
import com.realmofwrath.game.ecs.components.IdComponent;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.RenderableComponent;
import com.realmofwrath.game.ecs.components.SlotComponent;

public class DropBag extends Entity {

	public DropBag(int id, int ownerId, int x, int y, SlotComponent[] slots) {
		add(new IdComponent(id));
		add(new PositionComponent(x, y));
		//		add(new SpriteComponent(TextureManager.LOOT)); // FIXME Create sprite
		add(new RenderableComponent());
		add(new DropBagComponent(ownerId, slots));
	}

}