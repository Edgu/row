package com.realmofwrath.game.ecs;

import com.badlogic.ashley.core.ComponentMapper;
import com.realmofwrath.game.ecs.components.DropBagComponent;
import com.realmofwrath.game.ecs.components.HealthComponent;
import com.realmofwrath.game.ecs.components.IdComponent;
import com.realmofwrath.game.ecs.components.InventoryComponent;
import com.realmofwrath.game.ecs.components.ItemComponent;
import com.realmofwrath.game.ecs.components.MapComponent;
import com.realmofwrath.game.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.ecs.components.NPCComponent;
import com.realmofwrath.game.ecs.components.NameComponent;
import com.realmofwrath.game.ecs.components.PlayerComponent;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.SpriteComponent;
import com.realmofwrath.game.ecs.components.StateComponent;
import com.realmofwrath.game.ecs.components.VelocityComponent;

public class Mappers {

	public static final ComponentMapper<PositionComponent> POS_CM = ComponentMapper.getFor(PositionComponent.class);
	public static final ComponentMapper<SpriteComponent> SPR_CM = ComponentMapper.getFor(SpriteComponent.class);
	public static final ComponentMapper<VelocityComponent> VEL_CM = ComponentMapper.getFor(VelocityComponent.class);
	public static final ComponentMapper<MovementQueueComponent> MQU_CM = ComponentMapper.getFor(MovementQueueComponent.class);
	public static final ComponentMapper<StateComponent> STA_CM = ComponentMapper.getFor(StateComponent.class);
	public static final ComponentMapper<InventoryComponent> INV_CM = ComponentMapper.getFor(InventoryComponent.class);
	public static final ComponentMapper<NPCComponent> NPC_CM = ComponentMapper.getFor(NPCComponent.class);
	public static final ComponentMapper<PlayerComponent> PLA_CM = ComponentMapper.getFor(PlayerComponent.class);
	public static final ComponentMapper<HealthComponent> HP_CM = ComponentMapper.getFor(HealthComponent.class);
	public static final ComponentMapper<DropBagComponent> DB_CM = ComponentMapper.getFor(DropBagComponent.class);
	public static final ComponentMapper<IdComponent> ID_CM = ComponentMapper.getFor(IdComponent.class);
	public static final ComponentMapper<ItemComponent> ITE_CM = ComponentMapper.getFor(ItemComponent.class);
	public static final ComponentMapper<MapComponent> MAP_CM = ComponentMapper.getFor(MapComponent.class);
	public static final ComponentMapper<NameComponent> NAM_CM = ComponentMapper.getFor(NameComponent.class);

}