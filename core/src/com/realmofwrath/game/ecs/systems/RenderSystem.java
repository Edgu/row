package com.realmofwrath.game.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.SpriteComponent;
import com.realmofwrath.game.screens.GameScreen;

public class RenderSystem extends ReversedIteratingSystem {

	private SpriteBatch spriteBatch;
	private BitmapFont font;

	public RenderSystem(SpriteBatch spriteBatch, int priority) {
		super(Family.all(SpriteComponent.class, PositionComponent.class).get(), priority);
		setProcessing(false);
		this.spriteBatch = spriteBatch;
		font = new BitmapFont();
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		Vector2Integer entityPosition = Mappers.POS_CM.get(entity).position;

		Vector3 camPos = GameScreen.getCamera().position;
		Vector2Integer cameraPosition = new Vector2Integer((int) camPos.x - 16, (int) camPos.y - 16);

		Vector2Integer entityTilePosition = TiledUtils.positionToTiled(entityPosition);
		Vector2Integer cameraTilePosition = TiledUtils.positionToTiled(cameraPosition);

		int distanceX = Math.abs(entityTilePosition.x - cameraTilePosition.x);
		int distanceY = Math.abs(entityTilePosition.y - cameraTilePosition.y);

		if (distanceX <= 6 && distanceY <= 4) {
			spriteBatch.draw(Mappers.SPR_CM.get(entity).sprite, entityPosition.x, entityPosition.y);
			font.draw(spriteBatch, "x: " + entityPosition.x + ", y: " + entityPosition.y, entityPosition.x, entityPosition.y + 50);
			Vector2Integer tilePosition = TiledUtils.positionToTiled(entityPosition);
			font.draw(spriteBatch, "x: " + tilePosition.x + ", y: " + tilePosition.y, entityPosition.x, entityPosition.y + 70);
		}

	}

}