package com.realmofwrath.game.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.realmofwrath.game.custom.Constants;
import com.realmofwrath.game.custom.Map;
import com.realmofwrath.game.custom.TiledUtils;
import com.realmofwrath.game.custom.Vector2Integer;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.components.MapComponent;
import com.realmofwrath.game.ecs.components.MovementQueueComponent;
import com.realmofwrath.game.ecs.components.PositionComponent;
import com.realmofwrath.game.ecs.components.VelocityComponent;
import com.realmofwrath.game.screens.GameScreen;

public class MovementSystem extends IteratingSystem {

	public MovementSystem() {
		super(Family.all(PositionComponent.class, VelocityComponent.class, MovementQueueComponent.class).get());
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		Vector2Integer position = Mappers.POS_CM.get(entity).position;
		Vector2Integer velocity = Mappers.VEL_CM.get(entity).velocity;
		MovementQueueComponent mqc = Mappers.MQU_CM.get(entity);

		if (mqc.currentMove != null) {
			Vector2Integer mapPosition = TiledUtils.positionToMap(mqc.currentMove);
			if (velocity.x != 0) {
				if (mapPosition.x == position.x) {
					velocity.x = 0;
				}
			}

			if (velocity.y != 0) {
				if (mapPosition.y == position.y) {
					velocity.y = 0;
				}
			}

			if (mapPosition.equals(position)) {

				// Deal with warp crap
				MapComponent mc = Mappers.MAP_CM.get(entity);

				if (mc != null) {
					Map currentMap = GameScreen.maps.get(mc.mapId);
					WARP: {
						// Standing on warp tile.
						TiledMapTile tile = TiledUtils.getTile(currentMap.getMainLayer(), mqc.currentMove);
						if (tile.getProperties().containsKey("name")) {
							if (tile.getProperties().get("name").equals("warp")) {
								// Get WARP object.

								for (MapObject mapObject : currentMap.getObjectLayer().getObjects()) {
									if (mapPosition.equals(mapObject.getProperties().get("x", Float.class).intValue(), mapObject.getProperties().get("y", Float.class).intValue())) {
										int mapId = Integer.parseInt(mapObject.getProperties().get("destinationMap").toString());
										int objectId = Integer.parseInt(mapObject.getProperties().get("destinationObjectId").toString());

										Map destinationMap = GameScreen.maps.get(mapId);

										for (MapObject warpMapObject : destinationMap.getObjectLayer().getObjects()) {
											int warpMapObjectId = Integer.parseInt(warpMapObject.getProperties().get("id").toString());

											if (warpMapObjectId == objectId) {
												System.out.println("INITIATE WARP TO MAP: " + mapId);
												Mappers.MAP_CM.get(entity).mapId = mapId;

												position.x = warpMapObject.getProperties().get("x", Float.class).intValue();
												position.y = warpMapObject.getProperties().get("y", Float.class).intValue();

												mqc.queue.clear();
												break WARP;
											}
										}
									}
								}
							}
						}
					}
				}
				mqc.currentMove = null;
			}

		}

		if (!mqc.queue.isEmpty()) {
			if (mqc.currentMove == null) {
				Vector2Integer nextMove = mqc.queue.poll();
				Vector2Integer tiledPosition = TiledUtils.positionToTiled(position);
				mqc.currentMove = new Vector2Integer(tiledPosition.x + nextMove.x, tiledPosition.y + nextMove.y);
				velocity.x = nextMove.x;
				velocity.y = nextMove.y;
			}
		}

		position.x += velocity.x * Constants.MOVEMENT_SPEED;
		position.y += velocity.y * Constants.MOVEMENT_SPEED;
	}

}