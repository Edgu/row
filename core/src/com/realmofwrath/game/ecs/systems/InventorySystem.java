package com.realmofwrath.game.ecs.systems;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.realmofwrath.game.custom.Constants;
import com.realmofwrath.game.ecs.Mappers;
import com.realmofwrath.game.ecs.components.InventoryComponent;
import com.realmofwrath.game.ecs.components.SlotComponent;
import com.realmofwrath.game.ecs.entities.Item;

public abstract class InventorySystem extends EntitySystem {

	public static void setItem(Entity entity, int slotId, Item item) {
		if (item != null) {
			InventoryComponent ic = Mappers.INV_CM.get(entity);
			ic.slots.get(slotId).item = item;
		}
	}

	public static List<SlotComponent> createPlayerInventory() {
		List<SlotComponent> slots = new ArrayList<SlotComponent>();

		for (int i = 0; i < Constants.INVENTORY_SLOTS_AMOUNT; i++) {
			slots.add(new SlotComponent());
		}

		return slots;
	}

	public static void clearSlot(Entity entity, int slotId) {
		Mappers.INV_CM.get(entity).slots.get(slotId).item = null;
	}

	public static void swapSlots(Entity entity, int slotOneId, int slotTwoId) {
		InventoryComponent ic = Mappers.INV_CM.get(entity);

		Item first = ic.slots.get(slotOneId).item;

		if (first != null) {
			Item second = ic.slots.get(slotTwoId).item;

			clearSlot(entity, slotOneId);
			clearSlot(entity, slotTwoId);

			setItem(entity, slotOneId, second);
			setItem(entity, slotTwoId, first);
		}
	}

}