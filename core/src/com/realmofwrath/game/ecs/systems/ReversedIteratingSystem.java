package com.realmofwrath.game.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;

public abstract class ReversedIteratingSystem extends IteratingSystem {

	public ReversedIteratingSystem(Family family) {
		super(family);
	}
	
	public ReversedIteratingSystem(Family family, int priority) {
		super(family, priority);
	}
	
	@Override
	public void update(float deltaTime) {
		ImmutableArray<Entity> entities = getEntities();
		
		for (int i = entities.size() - 1; i >= 0; i--) {
			processEntity(entities.get(i), deltaTime);
		}
	}

}